const validarDatos = require('../middlewares/validarDatos');
const validarArchivo = require('../middlewares/validarArchivo');

module.exports = {
    ...validarDatos,
    ...validarArchivo
}
