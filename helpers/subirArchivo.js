const path = require('path');
const fs   = require('fs');

const subirArchivo = ( files, carpeta = '' ) => {
    return new Promise( async (resolve, reject) => {
       if ( files.archivo.length === undefined ){
        const { archivo } = files;
        const uploadPath = path.join( __dirname, '../uploads/', carpeta, archivo.name );
        archivo.mv(uploadPath, (err) => {
            if (err) {
                reject(err);
            }
            resolve( true );
        });
       } else {
        for( const file of files.archivo ){
            const uploadPath = path.join( __dirname, '../uploads/', carpeta, file.name );
            await file.mv(uploadPath, (err) => {
                if (err) {
                    reject(err);
                }
               
            });
        }
        resolve( true );
       }
    });
}

const getFilesDir = async (id) => {
    return new Promise( async (resolve, reject) => {
        resolve( fs.readdirSync(path.join( __dirname, '../uploads/', id )));
    });
    
  }

module.exports = {
    subirArchivo,
    getFilesDir
}