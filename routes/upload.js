const { Router } = require('express');
const { check } = require('express-validator');
const { cargarArchivo, getFiles } = require('../controllers/upload');
const { validarCampos, validarArchivoSubir } = require('../middlewares/middlewares');

const router = Router();

router.post( '/:id', validarArchivoSubir, cargarArchivo );

router.get( '/:id', getFiles );

module.exports = router;
