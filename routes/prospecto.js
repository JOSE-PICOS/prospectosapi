const { Router } = require('express');
const { check } = require('express-validator');
const { create, findAll, findById, rejectProspecto, checkProspecto } = require('../controllers/prospecto');
const { validarDatos } = require('../middlewares/middlewares');
const router = Router();

router.get('/', findAll );

router.get('/:id', findById);

router.post('/',[
    check('nombre', 'El nombre es obligatorio').not().isEmpty(),
    check('apellido_paterno', 'El apellido paterno es olbigatorio').not().isEmpty(),
    check('calle', 'La calle es obligatorio').not().isEmpty(),
    check('numero', 'El número es obligatorio').not().isEmpty(),
    check('colonia', 'La colonia es obligatorio').not().isEmpty(),
    check('codigo_postal', 'El nombre es obligatorio').not().isEmpty(),
    check('codigo_postal', 'El de código postal contener 5 caracteres').isLength({max:5 , min:5}),
    check('telefono', 'El nombre es obligatorio').not().isEmpty(),
    check('rfc', 'El RFC es obligatorio').not().isEmpty(),
    check('rfc', 'El RFC no tiene un formato valido').matches(/^([A-ZÑ&]{3,4}) ?(?:- ?)?(\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])) ?(?:- ?)?([A-Z\d]{2})([A\d])$/),
    validarDatos
], create );

router.patch('/rechazar-prospecto/:id',
[check('comentario_rechazo', 'Es obligatario un comentario de rechazo').not().isEmpty(), validarDatos]
, rejectProspecto);

router.patch('/autorizar-prospecto/:id', checkProspecto);

module.exports = router;
