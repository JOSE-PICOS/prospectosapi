
const {Schema, model} = require('mongoose');

const ProspectoSchema = Schema({
    nombre: {
        type: String,
        required: [true, 'El nombre es obligatorio']
    },
    apellido_paterno: {
        type: String,
        required: [true, 'El apellido paterno es olbigatorio']
    },
    apellido_materno: {
        type: String,
        required: [false]
    },
    calle: {
        type: String,
        required: [true, 'La calle es obligatorio']
    },
    numero: {
        type: String,
        required: [true, 'El número es obligatorio']
    },
    colonia: {
        type: String,
        required: [true, 'La colonia es obligatorio']
    },
    codigo_postal: {
        type: Number,
        required: [true, 'El código postal es obligatorio']
    },
    telefono: {
        type: String,
        required: [true, 'El teléfono es obligatorio']
    },
    rfc: {
        type: String,
        required: [true, 'El RFC es obligatorio']
    },
    status: {
        type: Number,
        required: [true, "El status es obligatorio"]
    },
    comentario_rechazo: {
        type: String,
        required: [false]
    }
});

ProspectoSchema.methods.toJSON = function() {
    const { __v, ...prospecto  } = this.toObject();
    // prospecto.uid = _id;
    return prospecto;
}

module.exports = model('Prospecto', ProspectoSchema)
