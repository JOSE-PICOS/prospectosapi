const Server = require('./server');
const Prospecto = require('./prospecto');

module.exports = {
    Server,
    Prospecto
}
