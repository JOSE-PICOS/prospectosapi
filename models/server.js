const express = require('express');
const cors = require('cors');
const { dbConnection } = require('../database/config');
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');

class Server {

    constructor() {
        this.app  = express();
        this.port = process.env.PORT;
        const prefijo = process.env.PREFIJO;

        this.paths = {
            prospectos:   `/${prefijo}/prospectos`,
            uploads:      `/${prefijo}/upload`,
        }


        // Conectar a base de datos
        this.conectarDB();

        // Middlewares
        this.middlewares();

        // Rutas de mi aplicación
        this.routes();
    }

    async conectarDB() {
        await dbConnection();
    }

    middlewares() {

        // CORS
        this.app.use( cors() );

        // Lectura y parseo del body
        this.app.use( express.json() );
        this.app.use(express.json({limit: '50mb'}));
        this.app.use(bodyParser.json({limit: '50mb'}));
        this.app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

        // Directorio Público
        this.app.use(express.static('uploads'));

        // Fileupload - Carga de archivos
        this.app.use( fileUpload({
            useTempFiles : true,
            tempFileDir : '/tmp/',
            createParentPath: true
        }));
    }

    routes() {
        this.app.use( this.paths.prospectos, require('../routes/prospecto'));
        this.app.use( this.paths.uploads, require('../routes/upload'));
        this.app.use('/static', express.static('uploads'));
    }

    listen() {
        this.app.listen( this.port, () => {
            console.log('Servidor corriendo en puerto', this.port );
        });
    }
}

module.exports = Server;
