const path = require('path');
const fs   = require('fs');

const { response } = require('express');
const { subirArchivo, getFilesDir } = require('../helpers/subirArchivo');

const cargarArchivo = async(req, res = response) => {
    try {
        const { id } = req.params;
        // txt, md
        // const nombre = await subirArchivo( req.files, ['txt','md'], 'textos' );
        const resp = await subirArchivo( req.files, id );
        if (resp){
            return res.json( {message: 'Carga de archivos exitosa.'} )
        }
        res.json({ message: 'No se pudieron cargar los archivos.' });

    } catch (msg) {
        res.status(400).json({ msg });
    }
}

const getFiles = async(req, res = response) => {
    try {
        const { id } = req.params;
        const result = await getFilesDir(id)
        res.json(result);
    } catch (msg) {
        res.status(400).json({ msg });
    }
}

module.exports = {
    cargarArchivo,
    getFiles
}
