const { response, request } = require('express');
const { ObjectId } = require('mongoose').Types;
const Prospecto = require('../models/prospecto');

const findAll = async(req = request, res = response) => {

    const { pageSize = 5, page = 0 } = req.query;
    
    const [ total, prospectos ] = await Promise.all([
        Prospecto.countDocuments(),
        Prospecto.find()
            .skip( Number( page ) )
            .limit(Number( pageSize ))
    ]);

    res.json({
        total,
        pageSize,
        page,
        prospectos
    });
}

const findById = async(req = request, res = response) => {

    const { id } = req.params;
    const esMongoID = ObjectId.isValid( id );
    if ( !esMongoID ){
        return res.status(500).json({
            message: 'El id no es valido...'
        });
    }

   const prospecto = await Prospecto.findById(id);

   res.json(prospecto);
}

const rejectProspecto = async(req, res = response) => {
    const {id} = req.params;
    const esMongoID = ObjectId.isValid( id );
    if ( !esMongoID ){
        return res.status(500).json({
            message: 'El id no es valido...'
        });
    }
    const {comentario_rechazo} = req.body;
    const prospecto = await Prospecto.findByIdAndUpdate(id, { comentario_rechazo, status: 3 });
    return res.json(prospecto);
}

const checkProspecto = async(req, res = response) => {
    const {id} = req.params;
    const esMongoID = ObjectId.isValid( id );
    if ( !esMongoID ){
        return res.status(500).json({
            message: 'El id no es valido...'
        });
    }
    const prospecto = await Prospecto.findByIdAndUpdate(id, {status: 2});
    return res.json(prospecto);
}

const create = async(req, res = response) => {
    
    const { nombre, apellido_paterno, apellido_materno, calle, numero, colonia, codigo_postal
            , telefono, rfc, status, comentario_rechazo } = req.body;
    const usuario = new Prospecto({ nombre, apellido_paterno, apellido_materno, calle, numero, colonia, codigo_postal
        , telefono, rfc, status, comentario_rechazo });

    // Guardar en BD
    await usuario.save();

    res.json({
        usuario
    });
}

module.exports = {
    create,
    findAll,
    findById,
    rejectProspecto,
    checkProspecto
}
